#!/usr/bin/env python

import json
import re
import requests

from StringIO import StringIO
from multiprocessing.pool import ThreadPool


WIKI_PRE = "https://en.wikipedia.org/w/index.php?title="
WIKI_POST = "&action=raw"
VDCS_JSON = "vdcs.json"
NUM_THREADS = 20

# some regular expressions to get the coordinates from wikipedia
rredirect = re.compile("#REDIRECT.*\[\[([^\]]*)\]\]", flags=re.IGNORECASE)
rlatdec = re.compile("\| *latd *= *([0-9\.]+) *"
                     "\| *latm *= *([0-9\.]+) *"
                     "\| *lats *= *([0-9\.]+) *"
                     "\| *latNS *= *([NS])", flags=re.IGNORECASE)
rlongdec = re.compile("\| *longd *= *([0-9\.]+) *"
                      "\| *longm *= *([0-9\.]+) *"
                      "\| *longs *= *([0-9\.]+) *"
                      "\| *longEW *= *([EW])", flags=re.IGNORECASE)
rlatd = re.compile("\| *latd *= *(-?[0-9\.]+)", flags=re.IGNORECASE)
rlongd = re.compile("\| *longd *= *(-?[0-9\.]+)", flags=re.IGNORECASE)
rcoord8 = re.compile("{{coord\|([0-9]+)\|([0-9]+)\|([0-9]+)\|([NS])"
                     "\|([0-9]+)\|([0-9]+)\|([0-9]+)\|([EW])",
                     flags=re.IGNORECASE)
rcoord6 = re.compile("{{coord\|([0-9]+)\|([0-9]+)\|([NS])"
                     "\|([0-9]+)\|([0-9]+)\|([EW])", flags=re.IGNORECASE)
rcoord4 = re.compile("{{coord\|([0-9\.]+)\|([NS])"
                     "\|([0-9\.]+)\|([EW])", flags=re.IGNORECASE)
rcoord2 = re.compile("{{coord\|(-?[0-9\.]+)\|(-?[0-9\.]+)",
                     flags=re.IGNORECASE)


def main():
    f = open(VDCS_JSON, 'r')
    vdcs = json.load(f)
    f.close()

    pool = ThreadPool(NUM_THREADS)
    coor = {}

    for region, zdict in vdcs.items():
        region = cleanName(region)
        coor[region] = {}
        for zone, ddict in zdict.items():
            zone = cleanName(zone)
            coor[region][zone] = {}
            for district, vlist in ddict.items():
                district = cleanName(district)
                coor[region][zone][district] = {}

                def proccessVillage(village):
                    vn = cleanName(village)
                    coor[region][zone][district][vn] = getLatLong(village)
                    print village, coor[region][zone][district][vn]

                pool.map(proccessVillage, vlist)
    print json.dumps(coor, indent=4)


def cleanName(name):
    return name.split(',')[0]


def getLatLong(village):
    req = requests.get(WIKI_PRE + village + WIKI_POST)
    if not req.ok:
        return [0, 0]

    lat = 0
    lon = 0
    entry = StringIO(req.content)
    for line in entry.readlines():
        r = rredirect.findall(line)
        if r:
            return getLatLong(r[0])

        r = rlatdec.findall(line)
        if r:
            lat = dms2dec(*r[0])
            continue
        r = rlatd.findall(line)
        if r:
            lat = float(r[0])
            continue

        r = rlongdec.findall(line)
        if r:
            lon = dms2dec(*r[0])
            continue
        r = rlongd.findall(line)
        if r:
            lon = float(r[0])
            continue

        r = rcoord8.findall(line)
        if r:
            lat = dms2dec(r[0][0], r[0][1], r[0][2], r[0][3])
            lon = dms2dec(r[0][4], r[0][5], r[0][6], r[0][7])
            continue
        r = rcoord6.findall(line)
        if r:
            lat = dms2dec(r[0][0], r[0][1], 0, r[0][2])
            lon = dms2dec(r[0][3], r[0][4], 0, r[0][5])
            continue
        r = rcoord4.findall(line)
        if r:
            lat = dms2dec(r[0][0], 0, 0, r[0][1])
            lon = dms2dec(r[0][2], 0, 0, r[0][3])
            continue
        r = rcoord2.findall(line)
        if r:
            lat = float(r[0][0])
            lon = float(r[0][1])
            continue

    return [lat, lon]


def dms2dec(degs, mins, secs, nsew):
    dec = float(degs)+(float(mins)/60.0)+(float(secs)/3600.0)
    if nsew in ["S", "W", "s", "w"]:
        dec = -1 * dec
    return dec


if __name__ == "__main__":
    main()
