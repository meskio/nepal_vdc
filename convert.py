#!/usr/bin/env python

import json
import sys


def main():
    f = open(sys.argv[1], 'r')
    vdcs = json.load(f)
    f.close()

    for region, zdict in vdcs.items():
        for zone, ddict in zdict.items():
            for district, vlist in ddict.items():
                for village, coor in vlist.items():
                    if coor[0] == 0 or coor[1] == 0:
                        continue
                    print (village + "," +
                           str(coor[0]) + "," +
                           str(coor[1]))


if __name__ == "__main__":
    main()
